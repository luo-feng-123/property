-- 记录表菜单

-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('来访人员', '2000', '1', 'record', 'community/record/index', 1, 0, 'C', '0', '0', 'community:record:list', '#', 'admin', sysdate(), '', null, '来访人员');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('来访人员', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'community:record:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('来访人员', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'community:record:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('来访人员', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'community:record:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('来访人员', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'community:record:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('来访人员', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'community:record:export',       '#', 'admin', sysdate(), '', null, '');