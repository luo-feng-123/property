
-- 删除入访记录表
DROP TABLE IF EXISTS wy_visit_record;
-- 创建入访记录表
CREATE TABLE wy_visit_record (
    visit_id INT PRIMARY KEY AUTO_INCREMENT COMMENT '入访序号',
    visitor_name VARCHAR(100) NOT NULL COMMENT '入访人',
    visitor_license_plate VARCHAR(15) COMMENT '入访车牌',
    visit_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT '入访时间',
    visitor_phone_number VARCHAR(15) NOT NULL COMMENT '入访手机号',
    remarks TEXT COMMENT '备注'
) COMMENT='记录来访人员的信息';