import request from '@/utils/request'

// 查询来访人员列表
export function listRecord(query) {
  return request({
    url: '/community/record/list',
    method: 'get',
    params: query
  })
}

// 查询来访人员详细
export function getRecord(visitId) {
  return request({
    url: '/community/record/' + visitId,
    method: 'get'
  })
}

// 新增来访人员
export function addRecord(data) {
  return request({
    url: '/community/record',
    method: 'post',
    data: data
  })
}

// 修改来访人员
export function updateRecord(data) {
  return request({
    url: '/community/record',
    method: 'put',
    data: data
  })
}

// 删除来访人员
export function delRecord(visitId) {
  return request({
    url: '/community/record/' + visitId,
    method: 'delete'
  })
}
