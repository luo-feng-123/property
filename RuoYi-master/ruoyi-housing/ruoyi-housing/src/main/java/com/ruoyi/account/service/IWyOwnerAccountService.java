package com.ruoyi.account.service;

import java.util.List;
import com.ruoyi.account.domain.WyOwnerAccount;

/**
 * 业主账户Service接口
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public interface IWyOwnerAccountService 
{
    /**
     * 查询业主账户
     * 
     * @param accountId 业主账户主键
     * @return 业主账户
     */
    public WyOwnerAccount selectWyOwnerAccountByAccountId(Long accountId);

    /**
     * 查询业主账户列表
     * 
     * @param wyOwnerAccount 业主账户
     * @return 业主账户集合
     */
    public List<WyOwnerAccount> selectWyOwnerAccountList(WyOwnerAccount wyOwnerAccount);

    /**
     * 新增业主账户
     * 
     * @param wyOwnerAccount 业主账户
     * @return 结果
     */
    public int insertWyOwnerAccount(WyOwnerAccount wyOwnerAccount);

    /**
     * 修改业主账户
     * 
     * @param wyOwnerAccount 业主账户
     * @return 结果
     */
    public int updateWyOwnerAccount(WyOwnerAccount wyOwnerAccount);

    /**
     * 批量删除业主账户
     * 
     * @param accountIds 需要删除的业主账户主键集合
     * @return 结果
     */
    public int deleteWyOwnerAccountByAccountIds(Long[] accountIds);

    /**
     * 删除业主账户信息
     * 
     * @param accountId 业主账户主键
     * @return 结果
     */
    public int deleteWyOwnerAccountByAccountId(Long accountId);
}
