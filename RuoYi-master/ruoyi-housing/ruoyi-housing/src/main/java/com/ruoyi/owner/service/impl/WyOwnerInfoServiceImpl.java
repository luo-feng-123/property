package com.ruoyi.owner.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.owner.mapper.WyOwnerInfoMapper;
import com.ruoyi.owner.domain.WyOwnerInfo;
import com.ruoyi.owner.service.IWyOwnerInfoService;

/**
 * 业主信息Service业务层处理
 * 
 * @author hdb
 * @date 2024-10-15
 */
@Service
public class WyOwnerInfoServiceImpl implements IWyOwnerInfoService 
{
    @Autowired
    private WyOwnerInfoMapper wyOwnerInfoMapper;

    /**
     * 查询业主信息
     * 
     * @param ownerId 业主信息主键
     * @return 业主信息
     */
    @Override
    public WyOwnerInfo selectWyOwnerInfoByOwnerId(Long ownerId)
    {
        return wyOwnerInfoMapper.selectWyOwnerInfoByOwnerId(ownerId);
    }

    /**
     * 查询业主信息列表
     * 
     * @param wyOwnerInfo 业主信息
     * @return 业主信息
     */
    @Override
    public List<WyOwnerInfo> selectWyOwnerInfoList(WyOwnerInfo wyOwnerInfo)
    {
        return wyOwnerInfoMapper.selectWyOwnerInfoList(wyOwnerInfo);
    }

    /**
     * 新增业主信息
     * 
     * @param wyOwnerInfo 业主信息
     * @return 结果
     */
    @Override
    public int insertWyOwnerInfo(WyOwnerInfo wyOwnerInfo)
    {
        return wyOwnerInfoMapper.insertWyOwnerInfo(wyOwnerInfo);
    }

    /**
     * 修改业主信息
     * 
     * @param wyOwnerInfo 业主信息
     * @return 结果
     */
    @Override
    public int updateWyOwnerInfo(WyOwnerInfo wyOwnerInfo)
    {
        return wyOwnerInfoMapper.updateWyOwnerInfo(wyOwnerInfo);
    }

    /**
     * 批量删除业主信息
     * 
     * @param ownerIds 需要删除的业主信息主键
     * @return 结果
     */
    @Override
    public int deleteWyOwnerInfoByOwnerIds(Long[] ownerIds)
    {
        return wyOwnerInfoMapper.deleteWyOwnerInfoByOwnerIds(ownerIds);
    }

    /**
     * 删除业主信息信息
     * 
     * @param ownerId 业主信息主键
     * @return 结果
     */
    @Override
    public int deleteWyOwnerInfoByOwnerId(Long ownerId)
    {
        return wyOwnerInfoMapper.deleteWyOwnerInfoByOwnerId(ownerId);
    }
}
