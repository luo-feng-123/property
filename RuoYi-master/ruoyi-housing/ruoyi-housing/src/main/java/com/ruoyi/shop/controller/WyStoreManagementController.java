package com.ruoyi.shop.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.shop.domain.WyStoreManagement;
import com.ruoyi.shop.service.IWyStoreManagementService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 商铺管理Controller
 * 
 * @author hdb
 * @date 2024-10-15
 */
@RestController
@RequestMapping("/housing/shop")
public class WyStoreManagementController extends BaseController
{
    @Autowired
    private IWyStoreManagementService wyStoreManagementService;

    /**
     * 查询商铺管理列表
     */
    @PreAuthorize("@ss.hasPermi('housing:shop:list')")
    @GetMapping("/list")
    public TableDataInfo list(WyStoreManagement wyStoreManagement)
    {
        startPage();
        List<WyStoreManagement> list = wyStoreManagementService.selectWyStoreManagementList(wyStoreManagement);
        return getDataTable(list);
    }

    /**
     * 导出商铺管理列表
     */
    @PreAuthorize("@ss.hasPermi('housing:shop:export')")
    @Log(title = "商铺管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WyStoreManagement wyStoreManagement)
    {
        List<WyStoreManagement> list = wyStoreManagementService.selectWyStoreManagementList(wyStoreManagement);
        ExcelUtil<WyStoreManagement> util = new ExcelUtil<WyStoreManagement>(WyStoreManagement.class);
        util.exportExcel(response, list, "商铺管理数据");
    }

    /**
     * 获取商铺管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('housing:shop:query')")
    @GetMapping(value = "/{shopId}")
    public AjaxResult getInfo(@PathVariable("shopId") Long shopId)
    {
        return success(wyStoreManagementService.selectWyStoreManagementByShopId(shopId));
    }

    /**
     * 新增商铺管理
     */
    @PreAuthorize("@ss.hasPermi('housing:shop:add')")
    @Log(title = "商铺管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WyStoreManagement wyStoreManagement)
    {
        return toAjax(wyStoreManagementService.insertWyStoreManagement(wyStoreManagement));
    }

    /**
     * 修改商铺管理
     */
    @PreAuthorize("@ss.hasPermi('housing:shop:edit')")
    @Log(title = "商铺管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WyStoreManagement wyStoreManagement)
    {
        return toAjax(wyStoreManagementService.updateWyStoreManagement(wyStoreManagement));
    }

    /**
     * 删除商铺管理
     */
    @PreAuthorize("@ss.hasPermi('housing:shop:remove')")
    @Log(title = "商铺管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{shopIds}")
    public AjaxResult remove(@PathVariable Long[] shopIds)
    {
        return toAjax(wyStoreManagementService.deleteWyStoreManagementByShopIds(shopIds));
    }
}
