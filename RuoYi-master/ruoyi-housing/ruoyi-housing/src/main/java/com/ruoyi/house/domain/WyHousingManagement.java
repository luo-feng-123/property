package com.ruoyi.house.domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.owner.domain.WyOwnerInfo;

/**
 * 房屋管理对象 wy_housing_management
 * 
 * @author hdb
 * @date 2024-10-15
 */
public class WyHousingManagement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID，主键 */
    private Long id;

    /** 楼层 */
    @Excel(name = "楼层")
    private String floor;

    /** 业主ID */
    @Excel(name = "业主ID")
    private Long ownerId;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 室内面积 */
    @Excel(name = "室内面积")
    private BigDecimal indoorArea;

    /** 租金 */
    @Excel(name = "租金")
    private BigDecimal rent;

    /** 房屋状态 */
    @Excel(name = "房屋状态")
    private String houseStatus;

    /** 入住时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入住时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date moveInTime;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 业主信息信息 */
    private List<WyOwnerInfo> wyOwnerInfoList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFloor(String floor) 
    {
        this.floor = floor;
    }

    public String getFloor() 
    {
        return floor;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setIndoorArea(BigDecimal indoorArea) 
    {
        this.indoorArea = indoorArea;
    }

    public BigDecimal getIndoorArea() 
    {
        return indoorArea;
    }
    public void setRent(BigDecimal rent) 
    {
        this.rent = rent;
    }

    public BigDecimal getRent() 
    {
        return rent;
    }
    public void setHouseStatus(String houseStatus) 
    {
        this.houseStatus = houseStatus;
    }

    public String getHouseStatus() 
    {
        return houseStatus;
    }
    public void setMoveInTime(Date moveInTime) 
    {
        this.moveInTime = moveInTime;
    }

    public Date getMoveInTime() 
    {
        return moveInTime;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    public List<WyOwnerInfo> getWyOwnerInfoList()
    {
        return wyOwnerInfoList;
    }

    public void setWyOwnerInfoList(List<WyOwnerInfo> wyOwnerInfoList)
    {
        this.wyOwnerInfoList = wyOwnerInfoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("floor", getFloor())
            .append("ownerId", getOwnerId())
            .append("type", getType())
            .append("indoorArea", getIndoorArea())
            .append("rent", getRent())
            .append("houseStatus", getHouseStatus())
            .append("moveInTime", getMoveInTime())
            .append("remarks", getRemarks())
            .append("wyOwnerInfoList", getWyOwnerInfoList())
            .toString();
    }
}
