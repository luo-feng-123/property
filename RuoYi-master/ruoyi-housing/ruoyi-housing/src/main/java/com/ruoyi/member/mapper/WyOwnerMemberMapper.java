package com.ruoyi.member.mapper;

import java.util.List;
import com.ruoyi.member.domain.WyOwnerMember;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业主成员Mapper接口
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public interface WyOwnerMemberMapper 
{
    /**
     * 查询业主成员
     * 
     * @param memberId 业主成员主键
     * @return 业主成员
     */
    public WyOwnerMember selectWyOwnerMemberByMemberId(Long memberId);

    /**
     * 查询业主成员列表
     * 
     * @param wyOwnerMember 业主成员
     * @return 业主成员集合
     */
    public List<WyOwnerMember> selectWyOwnerMemberList(WyOwnerMember wyOwnerMember);

    /**
     * 新增业主成员
     * 
     * @param wyOwnerMember 业主成员
     * @return 结果
     */
    public int insertWyOwnerMember(WyOwnerMember wyOwnerMember);

    /**
     * 修改业主成员
     * 
     * @param wyOwnerMember 业主成员
     * @return 结果
     */
    public int updateWyOwnerMember(WyOwnerMember wyOwnerMember);

    /**
     * 删除业主成员
     * 
     * @param memberId 业主成员主键
     * @return 结果
     */
    public int deleteWyOwnerMemberByMemberId(Long memberId);

    /**
     * 批量删除业主成员
     * 
     * @param memberIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerMemberByMemberIds(Long[] memberIds);

    /**
     * 批量删除业主信息
     * 
     * @param memberIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerIds(Long[] memberIds);
    
    /**
     * 批量新增业主信息
     * 
     * @param wyOwnerInfoList 业主信息列表
     * @return 结果
     */
    public int batchWyOwnerInfo(List<WyOwnerInfo> wyOwnerInfoList);
    

    /**
     * 通过业主成员主键删除业主信息信息
     * 
     * @param memberId 业主成员ID
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerId(Long memberId);
}
