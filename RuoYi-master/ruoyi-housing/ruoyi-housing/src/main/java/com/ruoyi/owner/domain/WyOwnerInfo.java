package com.ruoyi.owner.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 业主信息对象 wy_owner_info
 * 
 * @author hdb
 * @date 2024-10-15
 */
public class WyOwnerInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业主ID */
    private Long ownerId;

    /** 业主人脸 */
    @Excel(name = "业主人脸")
    private String faceImage;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 性别 */
    @Excel(name = "性别")
    private String gender;

    /** 身份证 */
    @Excel(name = "身份证")
    private String idCard;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String contactNumber;

    /** 家庭住址 */
    @Excel(name = "家庭住址")
    private String homeAddress;

    /** 门禁钥匙 */
    @Excel(name = "门禁钥匙")
    private String accessKey;

    /** 备注 */
    private String remarks;

    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setFaceImage(String faceImage) 
    {
        this.faceImage = faceImage;
    }

    public String getFaceImage() 
    {
        return faceImage;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getGender() 
    {
        return gender;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setContactNumber(String contactNumber) 
    {
        this.contactNumber = contactNumber;
    }

    public String getContactNumber() 
    {
        return contactNumber;
    }
    public void setHomeAddress(String homeAddress) 
    {
        this.homeAddress = homeAddress;
    }

    public String getHomeAddress() 
    {
        return homeAddress;
    }
    public void setAccessKey(String accessKey) 
    {
        this.accessKey = accessKey;
    }

    public String getAccessKey() 
    {
        return accessKey;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ownerId", getOwnerId())
            .append("faceImage", getFaceImage())
            .append("name", getName())
            .append("gender", getGender())
            .append("idCard", getIdCard())
            .append("contactNumber", getContactNumber())
            .append("homeAddress", getHomeAddress())
            .append("accessKey", getAccessKey())
            .append("remarks", getRemarks())
            .toString();
    }
}
