package com.ruoyi.account.mapper;

import java.util.List;
import com.ruoyi.account.domain.WyOwnerAccount;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业主账户Mapper接口
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public interface WyOwnerAccountMapper 
{
    /**
     * 查询业主账户
     * 
     * @param accountId 业主账户主键
     * @return 业主账户
     */
    public WyOwnerAccount selectWyOwnerAccountByAccountId(Long accountId);

    /**
     * 查询业主账户列表
     * 
     * @param wyOwnerAccount 业主账户
     * @return 业主账户集合
     */
    public List<WyOwnerAccount> selectWyOwnerAccountList(WyOwnerAccount wyOwnerAccount);

    /**
     * 新增业主账户
     * 
     * @param wyOwnerAccount 业主账户
     * @return 结果
     */
    public int insertWyOwnerAccount(WyOwnerAccount wyOwnerAccount);

    /**
     * 修改业主账户
     * 
     * @param wyOwnerAccount 业主账户
     * @return 结果
     */
    public int updateWyOwnerAccount(WyOwnerAccount wyOwnerAccount);

    /**
     * 删除业主账户
     * 
     * @param accountId 业主账户主键
     * @return 结果
     */
    public int deleteWyOwnerAccountByAccountId(Long accountId);

    /**
     * 批量删除业主账户
     * 
     * @param accountIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerAccountByAccountIds(Long[] accountIds);

    /**
     * 批量删除业主信息
     * 
     * @param accountIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerIds(Long[] accountIds);
    
    /**
     * 批量新增业主信息
     * 
     * @param wyOwnerInfoList 业主信息列表
     * @return 结果
     */
    public int batchWyOwnerInfo(List<WyOwnerInfo> wyOwnerInfoList);
    

    /**
     * 通过业主账户主键删除业主信息信息
     * 
     * @param accountId 业主账户ID
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerId(Long accountId);
}
