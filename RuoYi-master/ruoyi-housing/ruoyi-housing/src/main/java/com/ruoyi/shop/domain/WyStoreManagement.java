package com.ruoyi.shop.domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 商铺管理对象 wy_store_management
 * 
 * @author hdb
 * @date 2024-10-15
 */
public class WyStoreManagement extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 商铺ID */
    private Long shopId;

    /** 商铺编号 */
    @Excel(name = "商铺编号")
    private String shopNumber;

    /** 楼层 */
    @Excel(name = "楼层")
    private String floor;

    /** 业主ID */
    @Excel(name = "业主ID")
    private Long ownerId;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactNumber;

    /** 建筑面积 */
    @Excel(name = "建筑面积")
    private BigDecimal buildingArea;

    /** 室内面积 */
    @Excel(name = "室内面积")
    private BigDecimal indoorArea;

    /** 起租时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起租时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date leaseStartDate;

    /** 截租时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "截租时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date leaseEndDate;

    /** 租金 */
    @Excel(name = "租金")
    private BigDecimal rent;

    /** 商铺状态 */
    @Excel(name = "商铺状态")
    private String shopStatus;

    /** 备注 */
    private String remarks;

    /** 业主信息信息 */
    private List<WyOwnerInfo> wyOwnerInfoList;

    public void setShopId(Long shopId) 
    {
        this.shopId = shopId;
    }

    public Long getShopId() 
    {
        return shopId;
    }
    public void setShopNumber(String shopNumber) 
    {
        this.shopNumber = shopNumber;
    }

    public String getShopNumber() 
    {
        return shopNumber;
    }
    public void setFloor(String floor) 
    {
        this.floor = floor;
    }

    public String getFloor() 
    {
        return floor;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setContactNumber(String contactNumber) 
    {
        this.contactNumber = contactNumber;
    }

    public String getContactNumber() 
    {
        return contactNumber;
    }
    public void setBuildingArea(BigDecimal buildingArea) 
    {
        this.buildingArea = buildingArea;
    }

    public BigDecimal getBuildingArea() 
    {
        return buildingArea;
    }
    public void setIndoorArea(BigDecimal indoorArea) 
    {
        this.indoorArea = indoorArea;
    }

    public BigDecimal getIndoorArea() 
    {
        return indoorArea;
    }
    public void setLeaseStartDate(Date leaseStartDate) 
    {
        this.leaseStartDate = leaseStartDate;
    }

    public Date getLeaseStartDate() 
    {
        return leaseStartDate;
    }
    public void setLeaseEndDate(Date leaseEndDate) 
    {
        this.leaseEndDate = leaseEndDate;
    }

    public Date getLeaseEndDate() 
    {
        return leaseEndDate;
    }
    public void setRent(BigDecimal rent) 
    {
        this.rent = rent;
    }

    public BigDecimal getRent() 
    {
        return rent;
    }
    public void setShopStatus(String shopStatus) 
    {
        this.shopStatus = shopStatus;
    }

    public String getShopStatus() 
    {
        return shopStatus;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    public List<WyOwnerInfo> getWyOwnerInfoList()
    {
        return wyOwnerInfoList;
    }

    public void setWyOwnerInfoList(List<WyOwnerInfo> wyOwnerInfoList)
    {
        this.wyOwnerInfoList = wyOwnerInfoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("shopId", getShopId())
            .append("shopNumber", getShopNumber())
            .append("floor", getFloor())
            .append("ownerId", getOwnerId())
            .append("contactNumber", getContactNumber())
            .append("buildingArea", getBuildingArea())
            .append("indoorArea", getIndoorArea())
            .append("leaseStartDate", getLeaseStartDate())
            .append("leaseEndDate", getLeaseEndDate())
            .append("rent", getRent())
            .append("shopStatus", getShopStatus())
            .append("remarks", getRemarks())
            .append("wyOwnerInfoList", getWyOwnerInfoList())
            .toString();
    }
}
