package com.ruoyi.committee.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.committee.domain.WyOwnersCommittee;
import com.ruoyi.committee.service.IWyOwnersCommitteeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业委会Controller
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
@RestController
@RequestMapping("/housing/committee")
public class WyOwnersCommitteeController extends BaseController
{
    @Autowired
    private IWyOwnersCommitteeService wyOwnersCommitteeService;

    /**
     * 查询业委会列表
     */
    @PreAuthorize("@ss.hasPermi('housing:committee:list')")
    @GetMapping("/list")
    public TableDataInfo list(WyOwnersCommittee wyOwnersCommittee)
    {
        startPage();
        List<WyOwnersCommittee> list = wyOwnersCommitteeService.selectWyOwnersCommitteeList(wyOwnersCommittee);
        return getDataTable(list);
    }

    /**
     * 导出业委会列表
     */
    @PreAuthorize("@ss.hasPermi('housing:committee:export')")
    @Log(title = "业委会", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WyOwnersCommittee wyOwnersCommittee)
    {
        List<WyOwnersCommittee> list = wyOwnersCommitteeService.selectWyOwnersCommitteeList(wyOwnersCommittee);
        ExcelUtil<WyOwnersCommittee> util = new ExcelUtil<WyOwnersCommittee>(WyOwnersCommittee.class);
        util.exportExcel(response, list, "业委会数据");
    }

    /**
     * 获取业委会详细信息
     */
    @PreAuthorize("@ss.hasPermi('housing:committee:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wyOwnersCommitteeService.selectWyOwnersCommitteeById(id));
    }

    /**
     * 新增业委会
     */
    @PreAuthorize("@ss.hasPermi('housing:committee:add')")
    @Log(title = "业委会", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WyOwnersCommittee wyOwnersCommittee)
    {
        return toAjax(wyOwnersCommitteeService.insertWyOwnersCommittee(wyOwnersCommittee));
    }

    /**
     * 修改业委会
     */
    @PreAuthorize("@ss.hasPermi('housing:committee:edit')")
    @Log(title = "业委会", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WyOwnersCommittee wyOwnersCommittee)
    {
        return toAjax(wyOwnersCommitteeService.updateWyOwnersCommittee(wyOwnersCommittee));
    }

    /**
     * 删除业委会
     */
    @PreAuthorize("@ss.hasPermi('housing:committee:remove')")
    @Log(title = "业委会", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wyOwnersCommitteeService.deleteWyOwnersCommitteeByIds(ids));
    }
}
