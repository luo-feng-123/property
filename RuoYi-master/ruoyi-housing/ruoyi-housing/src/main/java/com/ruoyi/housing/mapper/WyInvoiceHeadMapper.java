package com.ruoyi.housing.mapper;

import java.util.List;
import com.ruoyi.housing.domain.WyInvoiceHead;

/**
 * 发票Mapper接口
 * 
 * @author hdb
 * @date 2024-10-14
 */
public interface WyInvoiceHeadMapper 
{
    /**
     * 查询发票
     * 
     * @param id 发票主键
     * @return 发票
     */
    public WyInvoiceHead selectWyInvoiceHeadById(Long id);

    /**
     * 查询发票列表
     * 
     * @param wyInvoiceHead 发票
     * @return 发票集合
     */
    public List<WyInvoiceHead> selectWyInvoiceHeadList(WyInvoiceHead wyInvoiceHead);

    /**
     * 新增发票
     * 
     * @param wyInvoiceHead 发票
     * @return 结果
     */
    public int insertWyInvoiceHead(WyInvoiceHead wyInvoiceHead);

    /**
     * 修改发票
     * 
     * @param wyInvoiceHead 发票
     * @return 结果
     */
    public int updateWyInvoiceHead(WyInvoiceHead wyInvoiceHead);

    /**
     * 删除发票
     * 
     * @param id 发票主键
     * @return 结果
     */
    public int deleteWyInvoiceHeadById(Long id);

    /**
     * 批量删除发票
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyInvoiceHeadByIds(Long[] ids);
}
