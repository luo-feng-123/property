package com.ruoyi.account.domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业主账户对象 wy_owner_account
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public class WyOwnerAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 账号 */
    @Excel(name = "账号")
    private Long accountId;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 业主ID */
    @Excel(name = "业主ID")
    private Long ownerId;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String idCard;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobileNumber;

    /** 账户类型 */
    @Excel(name = "账户类型")
    private String accountType;

    /** 账户金额 */
    @Excel(name = "账户金额")
    private BigDecimal accountBalance;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date creationTime;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 业主信息信息 */
    private List<WyOwnerInfo> wyOwnerInfoList;

    public void setAccountId(Long accountId) 
    {
        this.accountId = accountId;
    }

    public Long getAccountId() 
    {
        return accountId;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setMobileNumber(String mobileNumber) 
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() 
    {
        return mobileNumber;
    }
    public void setAccountType(String accountType) 
    {
        this.accountType = accountType;
    }

    public String getAccountType() 
    {
        return accountType;
    }
    public void setAccountBalance(BigDecimal accountBalance) 
    {
        this.accountBalance = accountBalance;
    }

    public BigDecimal getAccountBalance() 
    {
        return accountBalance;
    }
    public void setCreationTime(Date creationTime) 
    {
        this.creationTime = creationTime;
    }

    public Date getCreationTime() 
    {
        return creationTime;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    public List<WyOwnerInfo> getWyOwnerInfoList()
    {
        return wyOwnerInfoList;
    }

    public void setWyOwnerInfoList(List<WyOwnerInfo> wyOwnerInfoList)
    {
        this.wyOwnerInfoList = wyOwnerInfoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("accountId", getAccountId())
            .append("password", getPassword())
            .append("ownerId", getOwnerId())
            .append("idCard", getIdCard())
            .append("mobileNumber", getMobileNumber())
            .append("accountType", getAccountType())
            .append("accountBalance", getAccountBalance())
            .append("creationTime", getCreationTime())
            .append("remarks", getRemarks())
            .append("wyOwnerInfoList", getWyOwnerInfoList())
            .toString();
    }
}
