package com.ruoyi.member.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.member.mapper.WyOwnerMemberMapper;
import com.ruoyi.member.domain.WyOwnerMember;
import com.ruoyi.member.service.IWyOwnerMemberService;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业主成员Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
@Service
public class WyOwnerMemberServiceImpl implements IWyOwnerMemberService 
{
    @Autowired
    private WyOwnerMemberMapper wyOwnerMemberMapper;

    /**
     * 查询业主成员
     * 
     * @param memberId 业主成员主键
     * @return 业主成员
     */
    @Override
    public WyOwnerMember selectWyOwnerMemberByMemberId(Long memberId)
    {
        return wyOwnerMemberMapper.selectWyOwnerMemberByMemberId(memberId);
    }

    /**
     * 查询业主成员列表
     * 
     * @param wyOwnerMember 业主成员
     * @return 业主成员
     */
    @Override
    public List<WyOwnerMember> selectWyOwnerMemberList(WyOwnerMember wyOwnerMember)
    {
        return wyOwnerMemberMapper.selectWyOwnerMemberList(wyOwnerMember);
    }

    /**
     * 新增业主成员
     * 
     * @param wyOwnerMember 业主成员
     * @return 结果
     */
    @Transactional
    @Override
    public int insertWyOwnerMember(WyOwnerMember wyOwnerMember)
    {
        int rows = wyOwnerMemberMapper.insertWyOwnerMember(wyOwnerMember);
        insertWyOwnerInfo(wyOwnerMember);
        return rows;
    }

    /**
     * 修改业主成员
     * 
     * @param wyOwnerMember 业主成员
     * @return 结果
     */
    @Transactional
    @Override
    public int updateWyOwnerMember(WyOwnerMember wyOwnerMember)
    {
        wyOwnerMemberMapper.deleteWyOwnerInfoByOwnerId(wyOwnerMember.getMemberId());
        insertWyOwnerInfo(wyOwnerMember);
        return wyOwnerMemberMapper.updateWyOwnerMember(wyOwnerMember);
    }

    /**
     * 批量删除业主成员
     * 
     * @param memberIds 需要删除的业主成员主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyOwnerMemberByMemberIds(Long[] memberIds)
    {
        wyOwnerMemberMapper.deleteWyOwnerInfoByOwnerIds(memberIds);
        return wyOwnerMemberMapper.deleteWyOwnerMemberByMemberIds(memberIds);
    }

    /**
     * 删除业主成员信息
     * 
     * @param memberId 业主成员主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyOwnerMemberByMemberId(Long memberId)
    {
        wyOwnerMemberMapper.deleteWyOwnerInfoByOwnerId(memberId);
        return wyOwnerMemberMapper.deleteWyOwnerMemberByMemberId(memberId);
    }

    /**
     * 新增业主信息信息
     * 
     * @param wyOwnerMember 业主成员对象
     */
    public void insertWyOwnerInfo(WyOwnerMember wyOwnerMember)
    {
        List<WyOwnerInfo> wyOwnerInfoList = wyOwnerMember.getWyOwnerInfoList();
        Long memberId = wyOwnerMember.getMemberId();
        if (StringUtils.isNotNull(wyOwnerInfoList))
        {
            List<WyOwnerInfo> list = new ArrayList<WyOwnerInfo>();
            for (WyOwnerInfo wyOwnerInfo : wyOwnerInfoList)
            {
                wyOwnerInfo.setOwnerId(memberId);
                list.add(wyOwnerInfo);
            }
            if (list.size() > 0)
            {
                wyOwnerMemberMapper.batchWyOwnerInfo(list);
            }
        }
    }
}
