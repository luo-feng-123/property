package com.ruoyi.member.service;

import java.util.List;
import com.ruoyi.member.domain.WyOwnerMember;

/**
 * 业主成员Service接口
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public interface IWyOwnerMemberService 
{
    /**
     * 查询业主成员
     * 
     * @param memberId 业主成员主键
     * @return 业主成员
     */
    public WyOwnerMember selectWyOwnerMemberByMemberId(Long memberId);

    /**
     * 查询业主成员列表
     * 
     * @param wyOwnerMember 业主成员
     * @return 业主成员集合
     */
    public List<WyOwnerMember> selectWyOwnerMemberList(WyOwnerMember wyOwnerMember);

    /**
     * 新增业主成员
     * 
     * @param wyOwnerMember 业主成员
     * @return 结果
     */
    public int insertWyOwnerMember(WyOwnerMember wyOwnerMember);

    /**
     * 修改业主成员
     * 
     * @param wyOwnerMember 业主成员
     * @return 结果
     */
    public int updateWyOwnerMember(WyOwnerMember wyOwnerMember);

    /**
     * 批量删除业主成员
     * 
     * @param memberIds 需要删除的业主成员主键集合
     * @return 结果
     */
    public int deleteWyOwnerMemberByMemberIds(Long[] memberIds);

    /**
     * 删除业主成员信息
     * 
     * @param memberId 业主成员主键
     * @return 结果
     */
    public int deleteWyOwnerMemberByMemberId(Long memberId);
}
