package com.ruoyi.owner.mapper;

import java.util.List;
import com.ruoyi.owner.domain.WyOwnerInfo;

/**
 * 业主信息Mapper接口
 * 
 * @author hdb
 * @date 2024-10-15
 */
public interface WyOwnerInfoMapper 
{
    /**
     * 查询业主信息
     * 
     * @param ownerId 业主信息主键
     * @return 业主信息
     */
    public WyOwnerInfo selectWyOwnerInfoByOwnerId(Long ownerId);

    /**
     * 查询业主信息列表
     * 
     * @param wyOwnerInfo 业主信息
     * @return 业主信息集合
     */
    public List<WyOwnerInfo> selectWyOwnerInfoList(WyOwnerInfo wyOwnerInfo);

    /**
     * 新增业主信息
     * 
     * @param wyOwnerInfo 业主信息
     * @return 结果
     */
    public int insertWyOwnerInfo(WyOwnerInfo wyOwnerInfo);

    /**
     * 修改业主信息
     * 
     * @param wyOwnerInfo 业主信息
     * @return 结果
     */
    public int updateWyOwnerInfo(WyOwnerInfo wyOwnerInfo);

    /**
     * 删除业主信息
     * 
     * @param ownerId 业主信息主键
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerId(Long ownerId);

    /**
     * 批量删除业主信息
     * 
     * @param ownerIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerIds(Long[] ownerIds);
}
