package com.ruoyi.member.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.member.domain.WyOwnerMember;
import com.ruoyi.member.service.IWyOwnerMemberService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主成员Controller
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
@RestController
@RequestMapping("/housing/member")
public class WyOwnerMemberController extends BaseController
{
    @Autowired
    private IWyOwnerMemberService wyOwnerMemberService;

    /**
     * 查询业主成员列表
     */
    @PreAuthorize("@ss.hasPermi('housing:member:list')")
    @GetMapping("/list")
    public TableDataInfo list(WyOwnerMember wyOwnerMember)
    {
        startPage();
        List<WyOwnerMember> list = wyOwnerMemberService.selectWyOwnerMemberList(wyOwnerMember);
        return getDataTable(list);
    }

    /**
     * 导出业主成员列表
     */
    @PreAuthorize("@ss.hasPermi('housing:member:export')")
    @Log(title = "业主成员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WyOwnerMember wyOwnerMember)
    {
        List<WyOwnerMember> list = wyOwnerMemberService.selectWyOwnerMemberList(wyOwnerMember);
        ExcelUtil<WyOwnerMember> util = new ExcelUtil<WyOwnerMember>(WyOwnerMember.class);
        util.exportExcel(response, list, "业主成员数据");
    }

    /**
     * 获取业主成员详细信息
     */
    @PreAuthorize("@ss.hasPermi('housing:member:query')")
    @GetMapping(value = "/{memberId}")
    public AjaxResult getInfo(@PathVariable("memberId") Long memberId)
    {
        return success(wyOwnerMemberService.selectWyOwnerMemberByMemberId(memberId));
    }

    /**
     * 新增业主成员
     */
    @PreAuthorize("@ss.hasPermi('housing:member:add')")
    @Log(title = "业主成员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WyOwnerMember wyOwnerMember)
    {
        return toAjax(wyOwnerMemberService.insertWyOwnerMember(wyOwnerMember));
    }

    /**
     * 修改业主成员
     */
    @PreAuthorize("@ss.hasPermi('housing:member:edit')")
    @Log(title = "业主成员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WyOwnerMember wyOwnerMember)
    {
        return toAjax(wyOwnerMemberService.updateWyOwnerMember(wyOwnerMember));
    }

    /**
     * 删除业主成员
     */
    @PreAuthorize("@ss.hasPermi('housing:member:remove')")
    @Log(title = "业主成员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{memberIds}")
    public AjaxResult remove(@PathVariable Long[] memberIds)
    {
        return toAjax(wyOwnerMemberService.deleteWyOwnerMemberByMemberIds(memberIds));
    }
}
