package com.ruoyi.housing.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.housing.domain.WyInvoiceHead;
import com.ruoyi.housing.service.IWyInvoiceHeadService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 发票Controller
 * 
 * @author hdb
 * @date 2024-10-14
 */
@RestController
@RequestMapping("/invoice/invoice")
public class WyInvoiceHeadController extends BaseController
{
    @Autowired
    private IWyInvoiceHeadService wyInvoiceHeadService;

    /**
     * 查询发票列表
     */
    @PreAuthorize("@ss.hasPermi('invoice:invoice:list')")
    @GetMapping("/list")
    public TableDataInfo list(WyInvoiceHead wyInvoiceHead)
    {
        startPage();
        List<WyInvoiceHead> list = wyInvoiceHeadService.selectWyInvoiceHeadList(wyInvoiceHead);
        return getDataTable(list);
    }

    /**
     * 导出发票列表
     */
    @PreAuthorize("@ss.hasPermi('invoice:invoice:export')")
    @Log(title = "发票", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WyInvoiceHead wyInvoiceHead)
    {
        List<WyInvoiceHead> list = wyInvoiceHeadService.selectWyInvoiceHeadList(wyInvoiceHead);
        ExcelUtil<WyInvoiceHead> util = new ExcelUtil<WyInvoiceHead>(WyInvoiceHead.class);
        util.exportExcel(response, list, "发票数据");
    }

    /**
     * 获取发票详细信息
     */
    @PreAuthorize("@ss.hasPermi('invoice:invoice:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wyInvoiceHeadService.selectWyInvoiceHeadById(id));
    }

    /**
     * 新增发票
     */
    @PreAuthorize("@ss.hasPermi('invoice:invoice:add')")
    @Log(title = "发票", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WyInvoiceHead wyInvoiceHead)
    {
        return toAjax(wyInvoiceHeadService.insertWyInvoiceHead(wyInvoiceHead));
    }

    /**
     * 修改发票
     */
    @PreAuthorize("@ss.hasPermi('invoice:invoice:edit')")
    @Log(title = "发票", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WyInvoiceHead wyInvoiceHead)
    {
        return toAjax(wyInvoiceHeadService.updateWyInvoiceHead(wyInvoiceHead));
    }

    /**
     * 删除发票
     */
    @PreAuthorize("@ss.hasPermi('invoice:invoice:remove')")
    @Log(title = "发票", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wyInvoiceHeadService.deleteWyInvoiceHeadByIds(ids));
    }
}
