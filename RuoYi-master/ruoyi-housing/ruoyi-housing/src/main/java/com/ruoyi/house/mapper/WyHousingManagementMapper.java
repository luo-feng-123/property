package com.ruoyi.house.mapper;

import java.util.List;
import com.ruoyi.house.domain.WyHousingManagement;
import com.ruoyi.owner.domain.WyOwnerInfo;

/**
 * 房屋管理Mapper接口
 * 
 * @author hdb
 * @date 2024-10-15
 */
public interface WyHousingManagementMapper 
{
    /**
     * 查询房屋管理
     * 
     * @param id 房屋管理主键
     * @return 房屋管理
     */
    public WyHousingManagement selectWyHousingManagementById(Long id);

    /**
     * 查询房屋管理列表
     * 
     * @param wyHousingManagement 房屋管理
     * @return 房屋管理集合
     */
    public List<WyHousingManagement> selectWyHousingManagementList(WyHousingManagement wyHousingManagement);

    /**
     * 新增房屋管理
     * 
     * @param wyHousingManagement 房屋管理
     * @return 结果
     */
    public int insertWyHousingManagement(WyHousingManagement wyHousingManagement);

    /**
     * 修改房屋管理
     * 
     * @param wyHousingManagement 房屋管理
     * @return 结果
     */
    public int updateWyHousingManagement(WyHousingManagement wyHousingManagement);

    /**
     * 删除房屋管理
     * 
     * @param id 房屋管理主键
     * @return 结果
     */
    public int deleteWyHousingManagementById(Long id);

    /**
     * 批量删除房屋管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyHousingManagementByIds(Long[] ids);

    /**
     * 批量删除业主信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerIds(Long[] ids);
    
    /**
     * 批量新增业主信息
     * 
     * @param wyOwnerInfoList 业主信息列表
     * @return 结果
     */
    public int batchWyOwnerInfo(List<WyOwnerInfo> wyOwnerInfoList);
    

    /**
     * 通过房屋管理主键删除业主信息信息
     * 
     * @param id 房屋管理ID
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerId(Long id);
}
