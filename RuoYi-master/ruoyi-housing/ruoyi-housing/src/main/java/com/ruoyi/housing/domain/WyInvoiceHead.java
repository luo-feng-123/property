package com.ruoyi.housing.domain;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 发票对象 wy_invoice_head
 * 
 * @author hdb
 * @date 2024-10-14
 */
public class WyInvoiceHead extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 发票类型 */
    @Excel(name = "发票类型")
    private String invoiceType;

    /** 业主名称 */
    @Excel(name = "业主名称")
    private String ownerName;

    /** 开票人 */
    @Excel(name = "开票人")
    private String issuer;

    /** 发票名头 */
    @Excel(name = "发票名头")
    private String invoiceHeader;

    /** 纳税人识别号 */
    @Excel(name = "纳税人识别号")
    private String taxpayerId;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal amount;

    /** 发票号 */
    @Excel(name = "发票号")
    private String invoiceNumber;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private String auditStatus;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date applicationTime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setInvoiceType(String invoiceType) 
    {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceType() 
    {
        return invoiceType;
    }
    public void setOwnerName(String ownerName) 
    {
        this.ownerName = ownerName;
    }

    public String getOwnerName() 
    {
        return ownerName;
    }
    public void setIssuer(String issuer) 
    {
        this.issuer = issuer;
    }

    public String getIssuer() 
    {
        return issuer;
    }
    public void setInvoiceHeader(String invoiceHeader) 
    {
        this.invoiceHeader = invoiceHeader;
    }

    public String getInvoiceHeader() 
    {
        return invoiceHeader;
    }
    public void setTaxpayerId(String taxpayerId) 
    {
        this.taxpayerId = taxpayerId;
    }

    public String getTaxpayerId() 
    {
        return taxpayerId;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setInvoiceNumber(String invoiceNumber) 
    {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceNumber() 
    {
        return invoiceNumber;
    }
    public void setAuditStatus(String auditStatus) 
    {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatus() 
    {
        return auditStatus;
    }
    public void setApplicationTime(Date applicationTime) 
    {
        this.applicationTime = applicationTime;
    }

    public Date getApplicationTime() 
    {
        return applicationTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("invoiceType", getInvoiceType())
            .append("ownerName", getOwnerName())
            .append("issuer", getIssuer())
            .append("invoiceHeader", getInvoiceHeader())
            .append("taxpayerId", getTaxpayerId())
            .append("address", getAddress())
            .append("phone", getPhone())
            .append("amount", getAmount())
            .append("invoiceNumber", getInvoiceNumber())
            .append("auditStatus", getAuditStatus())
            .append("applicationTime", getApplicationTime())
            .toString();
    }
}
