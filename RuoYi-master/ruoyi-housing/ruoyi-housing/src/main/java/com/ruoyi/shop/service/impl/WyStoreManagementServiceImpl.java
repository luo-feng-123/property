package com.ruoyi.shop.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.shop.mapper.WyStoreManagementMapper;
import com.ruoyi.shop.domain.WyStoreManagement;
import com.ruoyi.shop.service.IWyStoreManagementService;
import com.ruoyi.owner.domain.WyOwnerInfo;

/**
 * 商铺管理Service业务层处理
 * 
 * @author hdb
 * @date 2024-10-15
 */
@Service
public class WyStoreManagementServiceImpl implements IWyStoreManagementService 
{
    @Autowired
    private WyStoreManagementMapper wyStoreManagementMapper;

    /**
     * 查询商铺管理
     * 
     * @param shopId 商铺管理主键
     * @return 商铺管理
     */
    @Override
    public WyStoreManagement selectWyStoreManagementByShopId(Long shopId)
    {
        return wyStoreManagementMapper.selectWyStoreManagementByShopId(shopId);
    }

    /**
     * 查询商铺管理列表
     * 
     * @param wyStoreManagement 商铺管理
     * @return 商铺管理
     */
    @Override
    public List<WyStoreManagement> selectWyStoreManagementList(WyStoreManagement wyStoreManagement)
    {
        return wyStoreManagementMapper.selectWyStoreManagementList(wyStoreManagement);
    }

    /**
     * 新增商铺管理
     * 
     * @param wyStoreManagement 商铺管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertWyStoreManagement(WyStoreManagement wyStoreManagement)
    {
        int rows = wyStoreManagementMapper.insertWyStoreManagement(wyStoreManagement);
        insertWyOwnerInfo(wyStoreManagement);
        return rows;
    }

    /**
     * 修改商铺管理
     * 
     * @param wyStoreManagement 商铺管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateWyStoreManagement(WyStoreManagement wyStoreManagement)
    {
        wyStoreManagementMapper.deleteWyOwnerInfoByOwnerId(wyStoreManagement.getShopId());
        insertWyOwnerInfo(wyStoreManagement);
        return wyStoreManagementMapper.updateWyStoreManagement(wyStoreManagement);
    }

    /**
     * 批量删除商铺管理
     * 
     * @param shopIds 需要删除的商铺管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyStoreManagementByShopIds(Long[] shopIds)
    {
        wyStoreManagementMapper.deleteWyOwnerInfoByOwnerIds(shopIds);
        return wyStoreManagementMapper.deleteWyStoreManagementByShopIds(shopIds);
    }

    /**
     * 删除商铺管理信息
     * 
     * @param shopId 商铺管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyStoreManagementByShopId(Long shopId)
    {
        wyStoreManagementMapper.deleteWyOwnerInfoByOwnerId(shopId);
        return wyStoreManagementMapper.deleteWyStoreManagementByShopId(shopId);
    }

    /**
     * 新增业主信息信息
     * 
     * @param wyStoreManagement 商铺管理对象
     */
    public void insertWyOwnerInfo(WyStoreManagement wyStoreManagement)
    {
        List<WyOwnerInfo> wyOwnerInfoList = wyStoreManagement.getWyOwnerInfoList();
        Long shopId = wyStoreManagement.getShopId();
        if (StringUtils.isNotNull(wyOwnerInfoList))
        {
            List<WyOwnerInfo> list = new ArrayList<WyOwnerInfo>();
            for (WyOwnerInfo wyOwnerInfo : wyOwnerInfoList)
            {
                wyOwnerInfo.setOwnerId(shopId);
                list.add(wyOwnerInfo);
            }
            if (list.size() > 0)
            {
                wyStoreManagementMapper.batchWyOwnerInfo(list);
            }
        }
    }
}
