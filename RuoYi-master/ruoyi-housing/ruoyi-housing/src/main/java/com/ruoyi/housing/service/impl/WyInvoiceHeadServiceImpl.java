package com.ruoyi.housing.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.housing.mapper.WyInvoiceHeadMapper;
import com.ruoyi.housing.domain.WyInvoiceHead;
import com.ruoyi.housing.service.IWyInvoiceHeadService;

/**
 * 发票Service业务层处理
 * 
 * @author hdb
 * @date 2024-10-14
 */
@Service
public class WyInvoiceHeadServiceImpl implements IWyInvoiceHeadService 
{
    @Autowired
    private WyInvoiceHeadMapper wyInvoiceHeadMapper;

    /**
     * 查询发票
     * 
     * @param id 发票主键
     * @return 发票
     */
    @Override
    public WyInvoiceHead selectWyInvoiceHeadById(Long id)
    {
        return wyInvoiceHeadMapper.selectWyInvoiceHeadById(id);
    }

    /**
     * 查询发票列表
     * 
     * @param wyInvoiceHead 发票
     * @return 发票
     */
    @Override
    public List<WyInvoiceHead> selectWyInvoiceHeadList(WyInvoiceHead wyInvoiceHead)
    {
        return wyInvoiceHeadMapper.selectWyInvoiceHeadList(wyInvoiceHead);
    }

    /**
     * 新增发票
     * 
     * @param wyInvoiceHead 发票
     * @return 结果
     */
    @Override
    public int insertWyInvoiceHead(WyInvoiceHead wyInvoiceHead)
    {
        //清空发票号
        wyInvoiceHead.setInvoiceNumber("");
        //设置发票号为uuid，并且设置长度为8位
        wyInvoiceHead.setInvoiceNumber(java.util.UUID.randomUUID().toString().replace("-", "").substring(0, 8));
        //设置发票时间为now()
        wyInvoiceHead.setApplicationTime(new java.util.Date());
        return wyInvoiceHeadMapper.insertWyInvoiceHead(wyInvoiceHead);
    }

    /**
     * 修改发票
     * 
     * @param wyInvoiceHead 发票
     * @return 结果
     */
    @Override
    public int updateWyInvoiceHead(WyInvoiceHead wyInvoiceHead)
    {
        //将发票的审核状态设置为待审核
        wyInvoiceHead.setAuditStatus("待审核");
        //将发票的审核状态设置为已通过
        wyInvoiceHead.setAuditStatus("已通过");
        return wyInvoiceHeadMapper.updateWyInvoiceHead(wyInvoiceHead);
    }

    /**
     * 批量删除发票
     * 
     * @param ids 需要删除的发票主键
     * @return 结果
     */
    @Override
    public int deleteWyInvoiceHeadByIds(Long[] ids)
    {
        return wyInvoiceHeadMapper.deleteWyInvoiceHeadByIds(ids);
    }

    /**
     * 删除发票信息
     * 
     * @param id 发票主键
     * @return 结果
     */
    @Override
    public int deleteWyInvoiceHeadById(Long id)
    {
        return wyInvoiceHeadMapper.deleteWyInvoiceHeadById(id);
    }
}
