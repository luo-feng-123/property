package com.ruoyi.committee.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.committee.mapper.WyOwnersCommitteeMapper;
import com.ruoyi.committee.domain.WyOwnersCommittee;
import com.ruoyi.committee.service.IWyOwnersCommitteeService;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业委会Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
@Service
public class WyOwnersCommitteeServiceImpl implements IWyOwnersCommitteeService 
{
    @Autowired
    private WyOwnersCommitteeMapper wyOwnersCommitteeMapper;

    /**
     * 查询业委会
     * 
     * @param id 业委会主键
     * @return 业委会
     */
    @Override
    public WyOwnersCommittee selectWyOwnersCommitteeById(Long id)
    {
        return wyOwnersCommitteeMapper.selectWyOwnersCommitteeById(id);
    }

    /**
     * 查询业委会列表
     * 
     * @param wyOwnersCommittee 业委会
     * @return 业委会
     */
    @Override
    public List<WyOwnersCommittee> selectWyOwnersCommitteeList(WyOwnersCommittee wyOwnersCommittee)
    {
        return wyOwnersCommitteeMapper.selectWyOwnersCommitteeList(wyOwnersCommittee);
    }

    /**
     * 新增业委会
     * 
     * @param wyOwnersCommittee 业委会
     * @return 结果
     */
    @Transactional
    @Override
    public int insertWyOwnersCommittee(WyOwnersCommittee wyOwnersCommittee)
    {
        int rows = wyOwnersCommitteeMapper.insertWyOwnersCommittee(wyOwnersCommittee);
        insertWyOwnerInfo(wyOwnersCommittee);
        return rows;
    }

    /**
     * 修改业委会
     * 
     * @param wyOwnersCommittee 业委会
     * @return 结果
     */
    @Transactional
    @Override
    public int updateWyOwnersCommittee(WyOwnersCommittee wyOwnersCommittee)
    {
        wyOwnersCommitteeMapper.deleteWyOwnerInfoByOwnerId(wyOwnersCommittee.getId());
        insertWyOwnerInfo(wyOwnersCommittee);
        return wyOwnersCommitteeMapper.updateWyOwnersCommittee(wyOwnersCommittee);
    }

    /**
     * 批量删除业委会
     * 
     * @param ids 需要删除的业委会主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyOwnersCommitteeByIds(Long[] ids)
    {
        wyOwnersCommitteeMapper.deleteWyOwnerInfoByOwnerIds(ids);
        return wyOwnersCommitteeMapper.deleteWyOwnersCommitteeByIds(ids);
    }

    /**
     * 删除业委会信息
     * 
     * @param id 业委会主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyOwnersCommitteeById(Long id)
    {
        wyOwnersCommitteeMapper.deleteWyOwnerInfoByOwnerId(id);
        return wyOwnersCommitteeMapper.deleteWyOwnersCommitteeById(id);
    }

    /**
     * 新增业主信息信息
     * 
     * @param wyOwnersCommittee 业委会对象
     */
    public void insertWyOwnerInfo(WyOwnersCommittee wyOwnersCommittee)
    {
        List<WyOwnerInfo> wyOwnerInfoList = wyOwnersCommittee.getWyOwnerInfoList();
        Long id = wyOwnersCommittee.getId();
        if (StringUtils.isNotNull(wyOwnerInfoList))
        {
            List<WyOwnerInfo> list = new ArrayList<WyOwnerInfo>();
            for (WyOwnerInfo wyOwnerInfo : wyOwnerInfoList)
            {
                wyOwnerInfo.setOwnerId(id);
                list.add(wyOwnerInfo);
            }
            if (list.size() > 0)
            {
                wyOwnersCommitteeMapper.batchWyOwnerInfo(list);
            }
        }
    }
}
