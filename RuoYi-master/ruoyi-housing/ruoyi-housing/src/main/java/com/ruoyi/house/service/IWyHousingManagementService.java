package com.ruoyi.house.service;

import java.util.List;
import com.ruoyi.house.domain.WyHousingManagement;

/**
 * 房屋管理Service接口
 * 
 * @author hdb
 * @date 2024-10-15
 */
public interface IWyHousingManagementService 
{
    /**
     * 查询房屋管理
     * 
     * @param id 房屋管理主键
     * @return 房屋管理
     */
    public WyHousingManagement selectWyHousingManagementById(Long id);

    /**
     * 查询房屋管理列表
     * 
     * @param wyHousingManagement 房屋管理
     * @return 房屋管理集合
     */
    public List<WyHousingManagement> selectWyHousingManagementList(WyHousingManagement wyHousingManagement);

    /**
     * 新增房屋管理
     * 
     * @param wyHousingManagement 房屋管理
     * @return 结果
     */
    public int insertWyHousingManagement(WyHousingManagement wyHousingManagement);

    /**
     * 修改房屋管理
     * 
     * @param wyHousingManagement 房屋管理
     * @return 结果
     */
    public int updateWyHousingManagement(WyHousingManagement wyHousingManagement);

    /**
     * 批量删除房屋管理
     * 
     * @param ids 需要删除的房屋管理主键集合
     * @return 结果
     */
    public int deleteWyHousingManagementByIds(Long[] ids);

    /**
     * 删除房屋管理信息
     * 
     * @param id 房屋管理主键
     * @return 结果
     */
    public int deleteWyHousingManagementById(Long id);
}
