package com.ruoyi.shop.mapper;

import java.util.List;
import com.ruoyi.shop.domain.WyStoreManagement;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 商铺管理Mapper接口
 * 
 * @author hdb
 * @date 2024-10-15
 */
public interface WyStoreManagementMapper 
{
    /**
     * 查询商铺管理
     * 
     * @param shopId 商铺管理主键
     * @return 商铺管理
     */
    public WyStoreManagement selectWyStoreManagementByShopId(Long shopId);

    /**
     * 查询商铺管理列表
     * 
     * @param wyStoreManagement 商铺管理
     * @return 商铺管理集合
     */
    public List<WyStoreManagement> selectWyStoreManagementList(WyStoreManagement wyStoreManagement);

    /**
     * 新增商铺管理
     * 
     * @param wyStoreManagement 商铺管理
     * @return 结果
     */
    public int insertWyStoreManagement(WyStoreManagement wyStoreManagement);

    /**
     * 修改商铺管理
     * 
     * @param wyStoreManagement 商铺管理
     * @return 结果
     */
    public int updateWyStoreManagement(WyStoreManagement wyStoreManagement);

    /**
     * 删除商铺管理
     * 
     * @param shopId 商铺管理主键
     * @return 结果
     */
    public int deleteWyStoreManagementByShopId(Long shopId);

    /**
     * 批量删除商铺管理
     * 
     * @param shopIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyStoreManagementByShopIds(Long[] shopIds);

    /**
     * 批量删除业主信息
     * 
     * @param shopIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerIds(Long[] shopIds);
    
    /**
     * 批量新增业主信息
     * 
     * @param wyOwnerInfoList 业主信息列表
     * @return 结果
     */
    public int batchWyOwnerInfo(List<WyOwnerInfo> wyOwnerInfoList);
    

    /**
     * 通过商铺管理主键删除业主信息信息
     * 
     * @param shopId 商铺管理ID
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerId(Long shopId);
}
