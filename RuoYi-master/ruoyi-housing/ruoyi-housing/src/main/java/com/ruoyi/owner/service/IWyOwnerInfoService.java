package com.ruoyi.owner.service;

import java.util.List;
import com.ruoyi.owner.domain.WyOwnerInfo;

/**
 * 业主信息Service接口
 * 
 * @author hdb
 * @date 2024-10-15
 */
public interface IWyOwnerInfoService 
{
    /**
     * 查询业主信息
     * 
     * @param ownerId 业主信息主键
     * @return 业主信息
     */
    public WyOwnerInfo selectWyOwnerInfoByOwnerId(Long ownerId);

    /**
     * 查询业主信息列表
     * 
     * @param wyOwnerInfo 业主信息
     * @return 业主信息集合
     */
    public List<WyOwnerInfo> selectWyOwnerInfoList(WyOwnerInfo wyOwnerInfo);

    /**
     * 新增业主信息
     * 
     * @param wyOwnerInfo 业主信息
     * @return 结果
     */
    public int insertWyOwnerInfo(WyOwnerInfo wyOwnerInfo);

    /**
     * 修改业主信息
     * 
     * @param wyOwnerInfo 业主信息
     * @return 结果
     */
    public int updateWyOwnerInfo(WyOwnerInfo wyOwnerInfo);

    /**
     * 批量删除业主信息
     * 
     * @param ownerIds 需要删除的业主信息主键集合
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerIds(Long[] ownerIds);

    /**
     * 删除业主信息信息
     * 
     * @param ownerId 业主信息主键
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerId(Long ownerId);
}
