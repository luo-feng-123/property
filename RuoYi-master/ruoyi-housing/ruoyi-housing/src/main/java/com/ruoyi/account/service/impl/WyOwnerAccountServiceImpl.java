package com.ruoyi.account.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.account.mapper.WyOwnerAccountMapper;
import com.ruoyi.account.domain.WyOwnerAccount;
import com.ruoyi.account.service.IWyOwnerAccountService;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业主账户Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
@Service
public class WyOwnerAccountServiceImpl implements IWyOwnerAccountService 
{
    @Autowired
    private WyOwnerAccountMapper wyOwnerAccountMapper;

    /**
     * 查询业主账户
     * 
     * @param accountId 业主账户主键
     * @return 业主账户
     */
    @Override
    public WyOwnerAccount selectWyOwnerAccountByAccountId(Long accountId)
    {
        return wyOwnerAccountMapper.selectWyOwnerAccountByAccountId(accountId);
    }

    /**
     * 查询业主账户列表
     * 
     * @param wyOwnerAccount 业主账户
     * @return 业主账户
     */
    @Override
    public List<WyOwnerAccount> selectWyOwnerAccountList(WyOwnerAccount wyOwnerAccount)
    {
        return wyOwnerAccountMapper.selectWyOwnerAccountList(wyOwnerAccount);
    }

    /**
     * 新增业主账户
     * 
     * @param wyOwnerAccount 业主账户
     * @return 结果
     */
    @Transactional
    @Override
    public int insertWyOwnerAccount(WyOwnerAccount wyOwnerAccount)
    {
        int rows = wyOwnerAccountMapper.insertWyOwnerAccount(wyOwnerAccount);
        insertWyOwnerInfo(wyOwnerAccount);
        return rows;
    }

    /**
     * 修改业主账户
     * 
     * @param wyOwnerAccount 业主账户
     * @return 结果
     */
    @Transactional
    @Override
    public int updateWyOwnerAccount(WyOwnerAccount wyOwnerAccount)
    {
        wyOwnerAccountMapper.deleteWyOwnerInfoByOwnerId(wyOwnerAccount.getAccountId());
        insertWyOwnerInfo(wyOwnerAccount);
        return wyOwnerAccountMapper.updateWyOwnerAccount(wyOwnerAccount);
    }

    /**
     * 批量删除业主账户
     * 
     * @param accountIds 需要删除的业主账户主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyOwnerAccountByAccountIds(Long[] accountIds)
    {
        wyOwnerAccountMapper.deleteWyOwnerInfoByOwnerIds(accountIds);
        return wyOwnerAccountMapper.deleteWyOwnerAccountByAccountIds(accountIds);
    }

    /**
     * 删除业主账户信息
     * 
     * @param accountId 业主账户主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyOwnerAccountByAccountId(Long accountId)
    {
        wyOwnerAccountMapper.deleteWyOwnerInfoByOwnerId(accountId);
        return wyOwnerAccountMapper.deleteWyOwnerAccountByAccountId(accountId);
    }

    /**
     * 新增业主信息信息
     * 
     * @param wyOwnerAccount 业主账户对象
     */
    public void insertWyOwnerInfo(WyOwnerAccount wyOwnerAccount)
    {
        List<WyOwnerInfo> wyOwnerInfoList = wyOwnerAccount.getWyOwnerInfoList();
        Long accountId = wyOwnerAccount.getAccountId();
        if (StringUtils.isNotNull(wyOwnerInfoList))
        {
            List<WyOwnerInfo> list = new ArrayList<WyOwnerInfo>();
            for (WyOwnerInfo wyOwnerInfo : wyOwnerInfoList)
            {
                wyOwnerInfo.setOwnerId(accountId);
                list.add(wyOwnerInfo);
            }
            if (list.size() > 0)
            {
                wyOwnerAccountMapper.batchWyOwnerInfo(list);
            }
        }
    }
}
