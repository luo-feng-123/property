package com.ruoyi.committee.mapper;

import java.util.List;
import com.ruoyi.committee.domain.WyOwnersCommittee;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业委会Mapper接口
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public interface WyOwnersCommitteeMapper 
{
    /**
     * 查询业委会
     * 
     * @param id 业委会主键
     * @return 业委会
     */
    public WyOwnersCommittee selectWyOwnersCommitteeById(Long id);

    /**
     * 查询业委会列表
     * 
     * @param wyOwnersCommittee 业委会
     * @return 业委会集合
     */
    public List<WyOwnersCommittee> selectWyOwnersCommitteeList(WyOwnersCommittee wyOwnersCommittee);

    /**
     * 新增业委会
     * 
     * @param wyOwnersCommittee 业委会
     * @return 结果
     */
    public int insertWyOwnersCommittee(WyOwnersCommittee wyOwnersCommittee);

    /**
     * 修改业委会
     * 
     * @param wyOwnersCommittee 业委会
     * @return 结果
     */
    public int updateWyOwnersCommittee(WyOwnersCommittee wyOwnersCommittee);

    /**
     * 删除业委会
     * 
     * @param id 业委会主键
     * @return 结果
     */
    public int deleteWyOwnersCommitteeById(Long id);

    /**
     * 批量删除业委会
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnersCommitteeByIds(Long[] ids);

    /**
     * 批量删除业主信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerIds(Long[] ids);
    
    /**
     * 批量新增业主信息
     * 
     * @param wyOwnerInfoList 业主信息列表
     * @return 结果
     */
    public int batchWyOwnerInfo(List<WyOwnerInfo> wyOwnerInfoList);
    

    /**
     * 通过业委会主键删除业主信息信息
     * 
     * @param id 业委会ID
     * @return 结果
     */
    public int deleteWyOwnerInfoByOwnerId(Long id);
}
