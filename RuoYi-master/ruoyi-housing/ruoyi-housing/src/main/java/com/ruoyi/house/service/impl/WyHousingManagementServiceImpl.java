package com.ruoyi.house.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.house.mapper.WyHousingManagementMapper;
import com.ruoyi.house.domain.WyHousingManagement;
import com.ruoyi.house.service.IWyHousingManagementService;
import com.ruoyi.owner.domain.WyOwnerInfo;

/**
 * 房屋管理Service业务层处理
 * 
 * @author hdb
 * @date 2024-10-15
 */
@Service
public class WyHousingManagementServiceImpl implements IWyHousingManagementService 
{
    @Autowired
    private WyHousingManagementMapper wyHousingManagementMapper;

    /**
     * 查询房屋管理
     * 
     * @param id 房屋管理主键
     * @return 房屋管理
     */
    @Override
    public WyHousingManagement selectWyHousingManagementById(Long id)
    {
        return wyHousingManagementMapper.selectWyHousingManagementById(id);
    }

    /**
     * 查询房屋管理列表
     * 
     * @param wyHousingManagement 房屋管理
     * @return 房屋管理
     */
    @Override
    public List<WyHousingManagement> selectWyHousingManagementList(WyHousingManagement wyHousingManagement)
    {
        return wyHousingManagementMapper.selectWyHousingManagementList(wyHousingManagement);
    }

    /**
     * 新增房屋管理
     * 
     * @param wyHousingManagement 房屋管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertWyHousingManagement(WyHousingManagement wyHousingManagement)
    {
        int rows = wyHousingManagementMapper.insertWyHousingManagement(wyHousingManagement);
        insertWyOwnerInfo(wyHousingManagement);
        return rows;
    }

    /**
     * 修改房屋管理
     * 
     * @param wyHousingManagement 房屋管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateWyHousingManagement(WyHousingManagement wyHousingManagement)
    {
        wyHousingManagementMapper.deleteWyOwnerInfoByOwnerId(wyHousingManagement.getId());
        insertWyOwnerInfo(wyHousingManagement);
        return wyHousingManagementMapper.updateWyHousingManagement(wyHousingManagement);
    }

    /**
     * 批量删除房屋管理
     * 
     * @param ids 需要删除的房屋管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyHousingManagementByIds(Long[] ids)
    {
        wyHousingManagementMapper.deleteWyOwnerInfoByOwnerIds(ids);
        return wyHousingManagementMapper.deleteWyHousingManagementByIds(ids);
    }

    /**
     * 删除房屋管理信息
     * 
     * @param id 房屋管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWyHousingManagementById(Long id)
    {
        wyHousingManagementMapper.deleteWyOwnerInfoByOwnerId(id);
        return wyHousingManagementMapper.deleteWyHousingManagementById(id);
    }

    /**
     * 新增业主信息信息
     * 
     * @param wyHousingManagement 房屋管理对象
     */
    public void insertWyOwnerInfo(WyHousingManagement wyHousingManagement)
    {
        List<WyOwnerInfo> wyOwnerInfoList = wyHousingManagement.getWyOwnerInfoList();
        Long id = wyHousingManagement.getId();
        if (StringUtils.isNotNull(wyOwnerInfoList))
        {
            List<WyOwnerInfo> list = new ArrayList<WyOwnerInfo>();
            for (WyOwnerInfo wyOwnerInfo : wyOwnerInfoList)
            {
                wyOwnerInfo.setOwnerId(id);
                list.add(wyOwnerInfo);
            }
            if (list.size() > 0)
            {
                wyHousingManagementMapper.batchWyOwnerInfo(list);
            }
        }
    }
}
