package com.ruoyi.member.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业主成员对象 wy_owner_member
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public class WyOwnerMember extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 成员ID */
    private Long memberId;

    /** 业主ID */
    @Excel(name = "业主ID")
    private Long ownerId;

    /** 成员人脸 */
    @Excel(name = "成员人脸")
    private String faceImage;

    /** 成员名称 */
    @Excel(name = "成员名称")
    private String memberName;

    /** 性别 */
    @Excel(name = "性别")
    private String gender;

    /** 类型 */
    @Excel(name = "类型")
    private String memberType;

    /** 身份证 */
    @Excel(name = "身份证")
    private String idCard;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobileNumber;

    /** 家庭住址 */
    @Excel(name = "家庭住址")
    private String homeAddress;

    /** 创建人 */
    @Excel(name = "创建人")
    private String creator;

    /** 门禁钥匙 */
    @Excel(name = "门禁钥匙")
    private String accessKey;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    /** 业主信息信息 */
    private List<WyOwnerInfo> wyOwnerInfoList;

    public void setMemberId(Long memberId) 
    {
        this.memberId = memberId;
    }

    public Long getMemberId() 
    {
        return memberId;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setFaceImage(String faceImage) 
    {
        this.faceImage = faceImage;
    }

    public String getFaceImage() 
    {
        return faceImage;
    }
    public void setMemberName(String memberName) 
    {
        this.memberName = memberName;
    }

    public String getMemberName() 
    {
        return memberName;
    }
    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getGender() 
    {
        return gender;
    }
    public void setMemberType(String memberType) 
    {
        this.memberType = memberType;
    }

    public String getMemberType() 
    {
        return memberType;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setMobileNumber(String mobileNumber) 
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() 
    {
        return mobileNumber;
    }
    public void setHomeAddress(String homeAddress) 
    {
        this.homeAddress = homeAddress;
    }

    public String getHomeAddress() 
    {
        return homeAddress;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }

    public String getCreator() 
    {
        return creator;
    }
    public void setAccessKey(String accessKey) 
    {
        this.accessKey = accessKey;
    }

    public String getAccessKey() 
    {
        return accessKey;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    public List<WyOwnerInfo> getWyOwnerInfoList()
    {
        return wyOwnerInfoList;
    }

    public void setWyOwnerInfoList(List<WyOwnerInfo> wyOwnerInfoList)
    {
        this.wyOwnerInfoList = wyOwnerInfoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("memberId", getMemberId())
            .append("ownerId", getOwnerId())
            .append("faceImage", getFaceImage())
            .append("memberName", getMemberName())
            .append("gender", getGender())
            .append("memberType", getMemberType())
            .append("idCard", getIdCard())
            .append("mobileNumber", getMobileNumber())
            .append("homeAddress", getHomeAddress())
            .append("creator", getCreator())
            .append("accessKey", getAccessKey())
            .append("remarks", getRemarks())
            .append("wyOwnerInfoList", getWyOwnerInfoList())
            .toString();
    }
}
