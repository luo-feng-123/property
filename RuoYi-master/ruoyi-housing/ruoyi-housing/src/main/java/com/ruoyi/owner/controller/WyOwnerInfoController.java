package com.ruoyi.owner.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.owner.domain.WyOwnerInfo;
import com.ruoyi.owner.service.IWyOwnerInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 业主信息Controller
 * 
 * @author hdb
 * @date 2024-10-15
 */
@RestController
@RequestMapping("/housing/owner")
public class WyOwnerInfoController extends BaseController
{
    @Autowired
    private IWyOwnerInfoService wyOwnerInfoService;

    /**
     * 查询业主信息列表
     */
    @PreAuthorize("@ss.hasPermi('housing:owner:list')")
    @GetMapping("/list")
    public TableDataInfo list(WyOwnerInfo wyOwnerInfo)
    {
        startPage();
        List<WyOwnerInfo> list = wyOwnerInfoService.selectWyOwnerInfoList(wyOwnerInfo);
        return getDataTable(list);
    }

    /**
     * 导出业主信息列表
     */
    @PreAuthorize("@ss.hasPermi('housing:owner:export')")
    @Log(title = "业主信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WyOwnerInfo wyOwnerInfo)
    {
        List<WyOwnerInfo> list = wyOwnerInfoService.selectWyOwnerInfoList(wyOwnerInfo);
        ExcelUtil<WyOwnerInfo> util = new ExcelUtil<WyOwnerInfo>(WyOwnerInfo.class);
        util.exportExcel(response, list, "业主信息数据");
    }

    /**
     * 获取业主信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('housing:owner:query')")
    @GetMapping(value = "/{ownerId}")
    public AjaxResult getInfo(@PathVariable("ownerId") Long ownerId)
    {
        return success(wyOwnerInfoService.selectWyOwnerInfoByOwnerId(ownerId));
    }

    /**
     * 新增业主信息
     */
    @PreAuthorize("@ss.hasPermi('housing:owner:add')")
    @Log(title = "业主信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WyOwnerInfo wyOwnerInfo)
    {
        return toAjax(wyOwnerInfoService.insertWyOwnerInfo(wyOwnerInfo));
    }

    /**
     * 修改业主信息
     */
    @PreAuthorize("@ss.hasPermi('housing:owner:edit')")
    @Log(title = "业主信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WyOwnerInfo wyOwnerInfo)
    {
        return toAjax(wyOwnerInfoService.updateWyOwnerInfo(wyOwnerInfo));
    }

    /**
     * 删除业主信息
     */
    @PreAuthorize("@ss.hasPermi('housing:owner:remove')")
    @Log(title = "业主信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ownerIds}")
    public AjaxResult remove(@PathVariable Long[] ownerIds)
    {
        return toAjax(wyOwnerInfoService.deleteWyOwnerInfoByOwnerIds(ownerIds));
    }
}
