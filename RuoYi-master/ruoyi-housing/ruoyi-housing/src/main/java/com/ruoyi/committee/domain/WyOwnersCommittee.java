package com.ruoyi.committee.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.owner.domain.WyOwnerInfo;
/**
 * 业委会对象 wy_owners_committee
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public class WyOwnersCommittee extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID，主键 */
    private Long id;

    /** 业主ID */
    @Excel(name = "业主ID")
    private Long ownerId;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 性别 */
    @Excel(name = "性别")
    private String gender;

    /** 电话 */
    @Excel(name = "电话")
    private String phone;

    /** 身份证 */
    @Excel(name = "身份证")
    private String idCard;

    /** 住址 */
    @Excel(name = "住址")
    private String address;

    /** 部门 */
    @Excel(name = "部门")
    private String department;

    /** 岗位 */
    @Excel(name = "岗位")
    private String position;

    /** 任期开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "任期开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date termStart;

    /** 任期结束日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "任期结束日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date termEnd;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 备注 */
    private String remarks;

    /** 业主信息信息 */
    private List<WyOwnerInfo> wyOwnerInfoList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getGender() 
    {
        return gender;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setDepartment(String department) 
    {
        this.department = department;
    }

    public String getDepartment() 
    {
        return department;
    }
    public void setPosition(String position) 
    {
        this.position = position;
    }

    public String getPosition() 
    {
        return position;
    }
    public void setTermStart(Date termStart) 
    {
        this.termStart = termStart;
    }

    public Date getTermStart() 
    {
        return termStart;
    }
    public void setTermEnd(Date termEnd) 
    {
        this.termEnd = termEnd;
    }

    public Date getTermEnd() 
    {
        return termEnd;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    public List<WyOwnerInfo> getWyOwnerInfoList()
    {
        return wyOwnerInfoList;
    }

    public void setWyOwnerInfoList(List<WyOwnerInfo> wyOwnerInfoList)
    {
        this.wyOwnerInfoList = wyOwnerInfoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ownerId", getOwnerId())
            .append("name", getName())
            .append("gender", getGender())
            .append("phone", getPhone())
            .append("idCard", getIdCard())
            .append("address", getAddress())
            .append("department", getDepartment())
            .append("position", getPosition())
            .append("termStart", getTermStart())
            .append("termEnd", getTermEnd())
            .append("status", getStatus())
            .append("remarks", getRemarks())
            .append("wyOwnerInfoList", getWyOwnerInfoList())
            .toString();
    }
}
