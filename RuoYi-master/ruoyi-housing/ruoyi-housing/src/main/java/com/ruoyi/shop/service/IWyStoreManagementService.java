package com.ruoyi.shop.service;

import java.util.List;
import com.ruoyi.shop.domain.WyStoreManagement;

/**
 * 商铺管理Service接口
 * 
 * @author hdb
 * @date 2024-10-15
 */
public interface IWyStoreManagementService 
{
    /**
     * 查询商铺管理
     * 
     * @param shopId 商铺管理主键
     * @return 商铺管理
     */
    public WyStoreManagement selectWyStoreManagementByShopId(Long shopId);

    /**
     * 查询商铺管理列表
     * 
     * @param wyStoreManagement 商铺管理
     * @return 商铺管理集合
     */
    public List<WyStoreManagement> selectWyStoreManagementList(WyStoreManagement wyStoreManagement);

    /**
     * 新增商铺管理
     * 
     * @param wyStoreManagement 商铺管理
     * @return 结果
     */
    public int insertWyStoreManagement(WyStoreManagement wyStoreManagement);

    /**
     * 修改商铺管理
     * 
     * @param wyStoreManagement 商铺管理
     * @return 结果
     */
    public int updateWyStoreManagement(WyStoreManagement wyStoreManagement);

    /**
     * 批量删除商铺管理
     * 
     * @param shopIds 需要删除的商铺管理主键集合
     * @return 结果
     */
    public int deleteWyStoreManagementByShopIds(Long[] shopIds);

    /**
     * 删除商铺管理信息
     * 
     * @param shopId 商铺管理主键
     * @return 结果
     */
    public int deleteWyStoreManagementByShopId(Long shopId);
}
