package com.ruoyi.house.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.house.domain.WyHousingManagement;
import com.ruoyi.house.service.IWyHousingManagementService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 房屋管理Controller
 * 
 * @author hdb
 * @date 2024-10-15
 */
@RestController
@RequestMapping("/housing/house")
public class WyHousingManagementController extends BaseController
{
    @Autowired
    private IWyHousingManagementService wyHousingManagementService;

    /**
     * 查询房屋管理列表
     */
    @PreAuthorize("@ss.hasPermi('housing:house:list')")
    @GetMapping("/list")
    public TableDataInfo list(WyHousingManagement wyHousingManagement)
    {
        startPage();
        List<WyHousingManagement> list = wyHousingManagementService.selectWyHousingManagementList(wyHousingManagement);
        return getDataTable(list);
    }

    /**
     * 导出房屋管理列表
     */
    @PreAuthorize("@ss.hasPermi('housing:house:export')")
    @Log(title = "房屋管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WyHousingManagement wyHousingManagement)
    {
        List<WyHousingManagement> list = wyHousingManagementService.selectWyHousingManagementList(wyHousingManagement);
        ExcelUtil<WyHousingManagement> util = new ExcelUtil<WyHousingManagement>(WyHousingManagement.class);
        util.exportExcel(response, list, "房屋管理数据");
    }

    /**
     * 获取房屋管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('housing:house:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wyHousingManagementService.selectWyHousingManagementById(id));
    }

    /**
     * 新增房屋管理
     */
    @PreAuthorize("@ss.hasPermi('housing:house:add')")
    @Log(title = "房屋管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WyHousingManagement wyHousingManagement)
    {
        return toAjax(wyHousingManagementService.insertWyHousingManagement(wyHousingManagement));
    }

    /**
     * 修改房屋管理
     */
    @PreAuthorize("@ss.hasPermi('housing:house:edit')")
    @Log(title = "房屋管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WyHousingManagement wyHousingManagement)
    {
        return toAjax(wyHousingManagementService.updateWyHousingManagement(wyHousingManagement));
    }

    /**
     * 删除房屋管理
     */
    @PreAuthorize("@ss.hasPermi('housing:house:remove')")
    @Log(title = "房屋管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wyHousingManagementService.deleteWyHousingManagementByIds(ids));
    }
}
