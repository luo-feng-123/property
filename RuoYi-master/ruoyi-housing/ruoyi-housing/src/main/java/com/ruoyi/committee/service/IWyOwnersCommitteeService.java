package com.ruoyi.committee.service;

import java.util.List;
import com.ruoyi.committee.domain.WyOwnersCommittee;

/**
 * 业委会Service接口
 * 
 * @author ruoyi
 * @date 2024-10-15
 */
public interface IWyOwnersCommitteeService 
{
    /**
     * 查询业委会
     * 
     * @param id 业委会主键
     * @return 业委会
     */
    public WyOwnersCommittee selectWyOwnersCommitteeById(Long id);

    /**
     * 查询业委会列表
     * 
     * @param wyOwnersCommittee 业委会
     * @return 业委会集合
     */
    public List<WyOwnersCommittee> selectWyOwnersCommitteeList(WyOwnersCommittee wyOwnersCommittee);

    /**
     * 新增业委会
     * 
     * @param wyOwnersCommittee 业委会
     * @return 结果
     */
    public int insertWyOwnersCommittee(WyOwnersCommittee wyOwnersCommittee);

    /**
     * 修改业委会
     * 
     * @param wyOwnersCommittee 业委会
     * @return 结果
     */
    public int updateWyOwnersCommittee(WyOwnersCommittee wyOwnersCommittee);

    /**
     * 批量删除业委会
     * 
     * @param ids 需要删除的业委会主键集合
     * @return 结果
     */
    public int deleteWyOwnersCommitteeByIds(Long[] ids);

    /**
     * 删除业委会信息
     * 
     * @param id 业委会主键
     * @return 结果
     */
    public int deleteWyOwnersCommitteeById(Long id);
}
